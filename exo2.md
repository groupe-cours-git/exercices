# Dépôts distants

Avant cet exercice, il faut avoir **créé son compte GitLab** et l'avoir
paramétré en ajoutant sa **clé SSH** (cf. le cours, ou chercher un tuto
sur internet).

1.  Créer un dépôt (*project/repository*) `Dépôt test distant` sur votre
    compte GitLab: créer un dépôt vierge, public. Retenir l'identifiant
    / *slug* (« *Project slug* ») du dépôt distant créé, normalement
    `depot-test-distant`.

2.  Cloner ce dépôt sur votre ordinateur (nécessite le paramétrage
    correct d'une clé SSH): utiliser
    `git clone git@gitlab.com:<votre login>/<slug du dépôt>`.

En fait, l'adresse complète du dépôt distant
`git@gitlab.com:<votre login>/<slug du dépôt>` peut aussi être obtenue
depuis GitLab en cliquant sur `Code` (bouton bleu sur la page du dépôt),
puis en copiant l'URL en haut de la zone qui apparaît (« Clone with SSH
»).

3.  La commande `git status` devrait maintenant vous indiquer une ligne
    supplémentaire (`Your branch is up to date with ...`).

**NB:** La synchronisation avec le dépôt distant n'est pas automatique,
donc il faut prendre cette indication avec des pincettes.

Une branche est un point de repère dans l'historique. Contrairement à un
*tag* (étiquette), une branche évolue au fil des commit que l'on fait :
si l'on est sur la branche `master` et que l'on rajoute un nouveau
commit, la branche `master` désignera ce dernier commit.

4.  Sur votre ordinateur, un dossier est apparu, du même nom que le
    *slug* du dépôt distant. Ce dossier est votre dépôt local. Modifier
    le fichier `README.md` qui se trouve à l'intérieur pour mettre le
    contenu du fichier `README.md` du premier exercice.

Remarquer que le fichier `README.md` s'affiche sur GitLab en
présentation du dépôt, formaté proprement. Pour plus de détails sur la
syntaxe `Markdown`, cf :

-   <https://docs.roadiz.io/fr/latest/user/write-in-markdown/> pour une
    entrée en matière
-   <https://daringfireball.net/projects/markdown/syntax> pour la
    syntaxe `Markdown` originale
-   <https://docs.gitlab.com/ee/user/markdown.html> pour le
    *GitLab-flavored Markdown*, version étendue de Markdown (en fait
    basée sur le *GitHub-flavored Markdown*)

6.  À cette étape, la commande `git status` indique toujours la même
    phrase, mais elle signale que le fichier `README.md` a été modifié.
    Valider le commit grâce à `git commit` (n'oubliez pas de mettre un
    message de commit) et vérifier ce qu'indique `git status` à nouveau.

A priori, elle indique que le dépôt local a un commit d'avance sur le
dépôt distant. Si vous n'avez pas ça, reprenez les étapes ci-dessus,
vous avez sans doute fait une erreur !

6.  Pour envoyer nos commits vers le serveur, on utiliser la commande
    `git push` (faites-le). Vérifier sur le site GitLab que le fichier
    `README.md` est bien mis à jour (rappel : il s'affiche sur la page
    principale du dépôt).

Vous n'avez pas d'adresse distante à spécifier car lors de l'opération
initiale `git clone`, la *branche principale* du dépôt local créée a été
paramétrée pour « pousser » automatiquement vers la *branche principale*
du dépôt distant.

On vient de voir comment envoyer des commits vers le dépôt distant avec
`git push`. Si vous utilisez Git pour collaborer sur un projet, au fil
du travail des modifications seront apportées au dépôt distant par vos
collègues. Voyons comment récupérer ces commits depuis le dépôt distant
vers votre dépôt local avec `git pull`.

7.  Tout d'abord, utiliser le « Web IDE » GitLab (depuis le dépôt
    GitLab, à côté du bouton `Code`, cliquer sur `Edit` \> `Web IDE`)
    pour modifier le fichier `README.md`. Pour valider dans un nouveau
    commit après des modifications, cliquer sur `Source Control` (bouton
    de la barre verticale gauche), puis `Commit to 'master'` (ou
    `'main'`), puis valider à nouveau, puis quitter le Web IDE.

8.  Utiliser maintenant `git pull` sur votre ordinateur pour télécharger
    le nouveau commit. Le fichier `README.md` présent sur votre
    ordinateur est modifié pour correspondre à la dernière version.

**Retenir** : *push* permet de pousser (des commits) vers un dépôt
distant, *pull* permet de tirer (des commits) vers le dépôt local.

9.  On va maintenant créer des *tags* pour désigner des versions du
    projet facilement. Taper `git log` pour afficher les SHA-1 de chaque
    commit. Copier les 6 premiers caractères (à peu près) du SHA-1 du
    commit initial, et taper `git tag v0 <SHA-1 du commit>`.

Le nom `v0` est au choix, vous pourriez mettre `debut` ou
`mon_tag_incroyable`. Vous devriez maintenant voir le tag apparaître
dans l'historique avec `git log`.

10. Créer un tag `v1` sur le commit actuel (pas besoin de SHA-1 ici...).

11. **Attention**: par défaut, les tags ne sont pas transférés lors d'un
    push. Transférer vos tags avec `git push --tags`.

Dans cet exercice, on a vu comment :

-   Cloner un dépôt distant pour en faire une copie locale
-   Pousser (*push*) des commits vers une branche distante
-   Tirer (*pull*) des commits vers sa branche locale

## Résoudre un conflit

On va voir qu'en plus de cloner des dépôts distants, on peut aussi les
enregistrer en tant que *remote* (littéralement, « \[dépôt\] distant »)
d'un dépôt local existant. Ces remotes ont des noms : lors d'un clone,
le remote `origin` est automatiquement créé.

12. Se déplacer dans le dépôt local créé lors de l'exercice 1 (dossier
    nommé `depot_test`), et taper
    `git remote add my_remote <adresse du depot distant>`, en utilisant
    le dépôt distant créé plus haut.
13. Ceci ajoute le remote nommé `my_remote`. Les historiques de ces
    dépôts n'ont rien à voir, mais on peut quand même les fusionner avec
    `git pull my_remote main --allow-unrelated-histories` (faites-le !).
14. Normalement Git vous signale un conflit dans `README.md`, car il
    existe mais il est dans un état différent dans chacun des projets.
    Ouvrir ce fichier avec un éditeur de texte, le modifier comme vous
    le souhaitez (vous pouvez garder des informations venant des deux
    projets : elles sont signalées par des lignes commençant par `>>>>`,
    `<<<<` et `====`).
15. Enfin, utiliser `git add README.md` pour valider les modifications,
    puis `git commit`. Puisque l'on est dans une étape de fusion
    (résolution de conflit), un message de commit est déjà prérempli.
    Valider directement (enregistrer et quitter).

Visualiser l'historique avec `git log` pour voir comment est représenté
la fusion d'historiques.

17. Enfin, vous pouvez envoyer ce commit vers le dépôt distant avec
    `git push -u my_remote main`. Ceci a pour effet additionnel de faire
    en sorte que votre branche locale « suive » la branche distante
    (comme si vous aviez cloné le projet). Ainsi les `git pull` et
    `git push` successifs peuvent se faire sans rien préciser d'autre.
