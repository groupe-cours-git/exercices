# Clones et Forks

On va commencer par voir comment supprimer un dépôt sur GitLab. Si vous
ne voulez pas supprimer votre premier et seul dépôt, créez-en un
nouveau.

1.  Depuis la page principale du dépôt à supprimer, aller dans
    « Settings \> General ». Puis, dans la section « Advanced », tout en
    bas → « Delete project ».

Remarque : c'est compliqué de supprimer un projet. GitLab cherche à
compliquer la tâche puisque alors on perd tout le travail effectué et
l'historique du projet. *À ne faire que si le projet ne contient pas de
travail (comme ici), sinon on risque de le regretter...*

Voyons maintenant comment l'on peut « proposer des commits » sur un
projet qui ne nous appartient pas.

2.  Dans votre répertoire de travail, taper
    `git clone git@gitlab.com:groupe-cours-git/exo-fork`.
3.  Créer un fichier nommé avec votre login GitLab et contenant ce que
    vous voulez.
4.  « Commit » vos changements, essayez de « push ». *NB : ça ne devrait
    pas marcher.*

Vous n'avez a priori pas de droits pour pousser sur mon dépôt ! Je
pourrais vous ajouter comme développeurs sur mon projet, mais on va
passer par le mécanisme des *merge requests*.

5.  Depuis la page de mon projet [Exo
    Fork](https://gitlab.com/groupe-cours-git/exo-fork), cliquer sur
    *fork* pour copier le dépôt sur votre compte.

Il y a maintenant une copie (ce qu'on appelle un *fork*) du projet
`exo-fork` dans votre espace GitLab personnel (je n'ai pas de droits sur
cette copie, vous n'avez pas de droits sur la mienne).

6.  Depuis votre dépôt local, créer un *remote* nommé `macopie` qui
    pointe vers *votre* copie (distante). *Cf. les dernières questions
    de l'exercice précédent pour la syntaxe...*
7.  Taper `git push -u macopie main`. *Rappel* : ceci va *pousser*
    vers le nouveau remote, et faire en sorte que la branche main
    locale *suive* la branche main du remote.
8.  Depuis GitLab, faire une nouvelle *merge request* de votre branche
    vers mon dépôt :

« Merge requests » → « New merge request », sélectionner *votre* branche
`main` comme source, *ma* branche `main` comme destination. Valider
avec « Create merge request ».

**Signalez-moi que vous avez fait votre requête, pour que je la
valide.**

9.  Après que votre *merge requests* ait été validée, vous vérifierez
    que mon dépôt est mis à jour en *pullant* : `git pull origin`
    (**NB**: puisque vous avez cloné mon dépôt, `origin` désigne mon
    dépôt).

*Puisque vous travaillez tous·tes sur le même dépôt, un fichier devra
apparaître pour chacun·e d'entre vous.*

**Dans cet exercice**, on a vu que :

-   Si l'on veut *pousser* vers un dépôt qui ne nous appartient pas, il
    faut passer par une *merge request*:
    -   D'abord dupliquer (*fork*) le dépôt sur son compte.
    -   Le cloner en local, faire ses modifications.
    -   Pousser vers son *fork*.
    -   Depuis GitLab, proposer la *merge request*.

## Pour aller plus loin

Avec GitLab, on peut créer directement un dépôt distant qui copie le
dépôt local (utile pour automatiser la création de nombreux dépôts à
partir de votre machine) :

    git remote add origin git@gitlab.com:<votre login>/nouveau_depot
    git push --set-upstream origin main

Ceci crée un nouveau dépôt `nouveau_depot` sur votre compte, si celui-ci
n'existe pas !
